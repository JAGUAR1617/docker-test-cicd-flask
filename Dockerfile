FROM python:3.10.8-slim-buster
ADD . /codes
WORKDIR /codes
RUN pip install -r requirements.txt
EXPOSE 80
CMD python run.py

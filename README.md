# docker-test-cicd-flask



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- take the latest pull and set your sshkey
- 

```
cd existing_repo
git clone https://gitlab.com/JAGUAR1617/docker-test-cicd-flask.git
git status
git add .
git commit -m" Your message"
git push origin <brqanch name> 
git push -uf origin main
```

## Integrate with your tools

- Integrate any tool which would help to improve performance of this application

## Test and Deploy

- cd tests and check for pytest script


## Name

Docker and kubernetes integration on python flask application

## Description
This project is mainly develope to integrate various algorithms in the main UI, where anyone can execute an algorithms on this app
for example sorting array, where user need to enter their array and in backend it uses the algorithm which can be displyed on UI also.


## Installation
Docker should be installed

## Usage
to execute this app: in terminal:---> docker-compose up

## Support

## Contributing

## Authors and acknowledgment

## License
For open source projects, say how it is licensed.

## Project status
Initial phase
from codes import app
from flask import render_template, request
from codes.helpers.helper import ArrayHelper

@app.route('/')
def layout():
    return render_template('layout.html', title='Main Page')

@app.route('/home')
def home():
    return render_template('home.html', title='Home')

@app.route('/sort_array', methods=['POST'])
def sort_array():
    input_array = request.form['inputArray']
    sorted_array = ArrayHelper.sort_array(input_array)

    return render_template('home.html', sorted_array=sorted_array)

@app.route('/index')
def index():
    return render_template('index.html', title='Index')
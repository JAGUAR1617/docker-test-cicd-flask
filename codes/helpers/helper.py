# array_helper.py
class ArrayHelper:
    @staticmethod
    def sort_array(input_array):
        try:
            array = [int(num.strip()) for num in input_array.split(',')]
            sorted_array = sorted(array)
            return sorted_array
        except ValueError:
            return None

# #!/bin/bash

# # Delete logs created by a test execution
# find ./logs -type f -delete

# # Remove empty directories inside ./screenshots
# find ./screenshots -type d -empty -delete

# echo "Post-cleanup script executed successfully."
